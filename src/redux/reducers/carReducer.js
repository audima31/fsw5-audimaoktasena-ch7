import { GET_ALL_CARS } from "../actions/carAction";

const initialState = {
  getAllCarsResult: false,
  getAllCarsLoading: false,
  getAllCarsError: false,
};

const carReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_CARS:
      return {
        ...state,
        getAllCarsResult: action.payload.data,
        getAllCarsLoading: action.payload.loading,
        getAllCarsError: action.payload.errorMessage,
      };
    default:
      return state;
  }
};

export default carReducer;
