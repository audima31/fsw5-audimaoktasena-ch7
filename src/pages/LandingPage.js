import React, { Component } from "react";
import { Content1 } from "../components/ContentComp/LandingPage/LpContent1";
import { Content2 } from "../components/ContentComp/LandingPage/LpContent2";
import { Content3 } from "../components/ContentComp/LandingPage/LpContent3";
import { Content4 } from "../components/ContentComp/LandingPage/LpContent4";
import { Content5 } from "../components/ContentComp/LandingPage/LpContent5";
import { Content6 } from "../components/ContentComp/LandingPage/LpContent6";
import { FooterComp } from "../components/FooterComp/FooterComp";
import { NavbarComp } from "../components/NavbarComp/NavbarComp";

export default class LandingPage extends Component {
  render() {
    return (
      <div>
        <NavbarComp />
        <Content1 />
        <Content2 />
        <Content3 />
        <Content4 />
        <Content5 />
        <Content6 />
        <FooterComp />
      </div>
    );
  }
}
