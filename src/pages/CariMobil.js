import React from "react";
import { CarContent1 } from "../components/ContentComp/CariMobil/CarContent1";
import { CarContent2 } from "../components/ContentComp/CariMobil/CarContent2";
import { FooterComp } from "../components/FooterComp/FooterComp";
import { NavbarComp } from "../components/NavbarComp/NavbarComp";

export const CariMobil = () => {
  return (
    <div>
      <NavbarComp />
      <CarContent1 />
      <CarContent2 />
      <FooterComp />
    </div>
  );
};
